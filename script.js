'use strict'

// ## Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// це технолоія, яка дозволяє веб сторінці взаємодіяти з веб-сервером без перезавантаження сторінки.

// ## Завдання
// Отримати список фільмів серії `Зоряні війни` та вивести на екран список персонажів для кожного з них.

// #### Технічні вимоги:
// - Надіслати AJAX запит на адресу `https://ajax.test-danit.com/api/swapi/films` та отримати список усіх фільмів серії `Зоряні війни`
// - Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості `characters`.
// - Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля `episodeId`, `name`, `openingCrawl`).
// - Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// #### Необов'язкове завдання підвищеної складності
//  - Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

const root = document.querySelector('#root')

const URL = 'https://ajax.test-danit.com/api/swapi/films'

class Film {
  request(url) {
    return fetch(url).then((response) => {
      return response.json()
    })
  }

  getFilms(url) {
    return this.request(url)
  }

  getCharacters(characters) {
    const charactersRequests = characters.map((character) => {
      return this.request(character)
    })

    return Promise.allSettled([...charactersRequests])
  }

  renderFilms(films) {
    let filmsList = document.createElement('div')
    let fragment = document.createDocumentFragment()
    films.map(({ episodeId, name, openingCrawl, characters }, index) => {
      let episod = document.createElement('p')
      let episodName = document.createElement('p')
      let episodCrawl = document.createElement('p')
      let charactersList = document.createElement('ul')
      episod.textContent = `Номер епізоду: ${episodeId}`
      episodName.textContent = `Назву фільму: ${name}`
      episodCrawl.textContent = `Короткий зміст: ${openingCrawl}`
      charactersList.append(this.preloader())

      fragment.append(episod, episodName, episodCrawl, charactersList)

      setTimeout(() => {
        this.getCharacters(characters).then((characters) => {
          charactersList.innerHTML = ''
          let characterItems = characters.map(({ value }) => {
            let characteItem = document.createElement('li')
            characteItem.textContent = value.name
            return characteItem
          })

          charactersList.append(...characterItems)
        })
      }, 1000 * (index + 1))

      return fragment
    })

    filmsList.append(fragment)

    return filmsList
  }

  preloader() {
    const preloaderDiv = document.createElement('div')
    preloaderDiv.classList.add('preloader')
    preloaderDiv.style.display = 'block'
    const spinnerDiv = document.createElement('div')
    spinnerDiv.classList.add('spinner')
    preloaderDiv.append(spinnerDiv)

    return preloaderDiv
  }
}

let film = new Film()

film
  .getFilms(URL)
  .then((films) => {
    root.append(film.renderFilms(films))
  })
  .catch((e) => {
    console.log(e)
  })
